gsp_start;
path = './csv_files/'
% read files
W = csvread(strcat(path,'W.csv'))
L = csvread(strcat(path,'L.csv'))
coords = csvread(strcat(path,'coords.csv'))

% make graph and fourier_basis
G = gsp_graph(W)
G = gsp_update_coordinates(G, coords)
G.L = L;
% gsp_plot_graph(G);
% saveas(gcf,'./master_images/graph.png')
G = gsp_compute_fourier_basis(G);
csvwrite(strcat(path,'eigens.csv'),G.e)

% read a signal
f = csvread(strcat(path,'f.csv'))

% Compute signal spectral
f_hat = gsp_gft(G,f)
csvwrite(strcat(path,'f_hat.csv'),f_hat)

% Create filter
% Hytan
tau = G.lmax/3;
g = @(x) ((1-exp(-20*(x-tau)))./(1+exp(-20*(x-tau))))*(1/2)+0.5;
% gsp_plot_filter(G,h);

% mexican hat 
% Nf = 8;
% g = gsp_design_mexican_hat(G, Nf);
% gsp_plot_filter(G,g);

% itersine filter
% Nf = 10;
% G = gsp_estimate_lmax(G);
% g = gsp_design_itersine(G, Nf);
% gsp_plot_filter(G,g);

% meyer filter
% Nf = 6;
% g = gsp_design_meyer(G, Nf);
% gsp_plot_filter(G,g(6));

% Plot 
%figure;
% subplot(211)
% gsp_plot_signal_spectral(G,f_hat);
% title('Signal Spectrum');
% subplot(212)
gsp_plot_filter(G,g);
title('High Pass Filter');
saveas(gcf,'./master_images/spectrum.png')

% Remove the noise
% f1 = gsp_filter(G,g(1),f);
% csvwrite(strcat(path,'f1.csv'),f1)
% f2 = gsp_filter(G,g(2),f);
% csvwrite(strcat(path,'f2.csv'),f2)
% f3 = gsp_filter(G,g(3),f);
% csvwrite(strcat(path,'f3.csv'),f3)
% f4 = gsp_filter(G,g(4),f);
% csvwrite(strcat(path,'f4.csv'),f4)
% f5 = gsp_filter(G,g(5),f);
% csvwrite(strcat(path,'f5.csv'),f5)
% f6 = gsp_filter(G,g(6),f);
% csvwrite(strcat(path,'f6.csv'),f6)
% f7 = gsp_filter(G,g(7),f);
% csvwrite(strcat(path,'f7.csv'),f7)
% f8 = gsp_filter(G,g(8),f);
% csvwrite(strcat(path,'f8.csv'),f8)
% f9 = gsp_filter(G,g(9),f);
% csvwrite(strcat(path,'f9.csv'),f9)
% f10 = gsp_filter(G,g(10),f);
% csvwrite(strcat(path,'f10.csv'),f10)
f2 = gsp_filter(G,g,f);
csvwrite(strcat(path,'f2.csv'),f2)

% Compute signal spectral
f2_hat=gsp_gft(G,f2);
csvwrite(strcat(path,'f2_hat.csv'),f2_hat)

x = linspace(0,3);
% Plot spectrals
%figure;
%subplot(311)
%gsp_plot_signal_spectral(G,f_hat);
%title('Power Spectrum');
%subplot(312)
%plot(x,g(x));
%title('High Pass Filter');
%subplot(313)
%gsp_plot_signal_spectral(G,f2_hat);
%title('High Pass Filtered Power Spectrum');
%saveas(gcf,'./master_images/filtered.png')
