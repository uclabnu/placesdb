# -*- coding: utf-8 -*-
"""
W.csv, coords.csv, f.csvの出力
エッジに距離の逆数でスコア付け
重み行列の正規化
scp -r yossi@133.6.254.14:./gsp/csv_files ./
"""

import csv
import pyproj
import numpy as np
import psycopg2
import urllib.parse as up
import datetime as dt
import math

# 地球の半径定義
earth_rad = 6378.137
geo_list = []
# 境目の日付を定義
update_keys = []
for i in range(1,10):
    update_keys.append('2017-12-0' + str(i))
for i in range(10,32):
    update_keys.append('2017-12-' + str(i))

# ガウス関数の定義　x/betaを入力することで横方向に拡大(2*beta = 100mが2σ)
sigma = 2
beta = 125
gauss = lambda x: (math.exp(-x**2/(2*sigma**2)) / math.sqrt(2*math.pi*sigma**2))

def make_update_ratio(shops_serial):
    cnt = 0
    for day in update_keys:
        begin = dt.datetime.strptime(day, '%Y-%m-%d')
        end = begin + dt.timedelta(days=1)
        cur.execute("SELECT distinct_alphanumeric FROM html_log WHERE update_time >= %s AND update_time <= %s AND shops_serial = %s;",[begin, end, shops_serial])
        ratio_tupple = cur.fetchone()
        if ratio_tupple:
            if ratio_tupple[0] != 1.0:
                cnt += 1
    return cnt/len(update_keys)

def latlng_to_xyz(lat, lng):
    rlat, rlng = math.radians(lat), math.radians(lng)
    coslat = math.cos(rlat)
    return coslat*math.cos(rlng), coslat*math.sin(rlng), math.sin(rlat)

def dist_on_sphere(pos0, pos1, radius=earth_rad):
    xyz0, xyz1 = latlng_to_xyz(*pos0), latlng_to_xyz(*pos1)
    if sum(x * y for x, y in zip(xyz0, xyz1)) > 1.0:
        value = 0.0
    else:
        value = math.acos(sum(x * y for x, y in zip(xyz0, xyz1)))*radius
    return value

def comupute_inverse_distance(start, end):
    # 距離0だとエラる可能性あり
    if start == end:
        distance = 0.0
    else:
        distance = dist_on_sphere(start, end)*1000    #kmをmに
    return gauss(distance/beta)

def normalization_matrix(W):
    size = len(W)
    norms = []
    # 行和を対角成分に
    for i in range(size):
        row_sum = np.sum(W[i])
        norms.append(math.sqrt(row_sum))
    # 行方向にnormで徐算
    for i in range(size):
        if norms[i] == 0:
            continue
        W[i] = W[i]/norms[i]
    # 列方向に√col_sumで徐算
    for i in range (size):
        for j in range(size):
            if norms[j] == 0:
                continue
            W[i][j] = W[i][j]/norms[j]
    W = -W
    np.fill_diagonal(W, 1.0)
    return W

# NG_set定義
NG_set = set(['r.gnavi.co.jp', 'tabelog.com', 'www.hotpepper.jp', 'ameblo.jp', 'www.bakuroichidai.co.jp', 'www.kanemi-foods.co.jp'])
def domain_checker(url):
    o = up.urlparse(url)
    return o.netloc in NG_set

# PostgreSQLに接続
try:
    conn = psycopg2.connect("dbname='placesdb' user='yossi' host='localhost' password='hoge'")
    cur = conn.cursor()
except:
    print("接続失敗")

start = dt.datetime.today()
print('searching nodes')
# カテゴリー付きWebページ（飲食店サーチ）
geo_nodes = []
cur.execute("SELECT serial FROM shops_hp WHERE category ~ 'restaurant';")
tmp_list = cur.fetchall()
for tmp in tmp_list:
    cur.execute("SELECT serial, shops_serial, lat, lng, url FROM shops_geo WHERE shops_serial = %s;",[tmp])
    hoge_list = cur.fetchall()
    for hoge in hoge_list:
        if domain_checker(hoge[4]):
            continue
        geo_nodes.append(hoge)

print("nodes len =", len(geo_nodes), flush=True)

# 計算時間計測
end = dt.datetime.today()
print("search nodes =", end - start, flush=True)
start = dt.datetime.today()

# signal, latlng, shops_listの作成
print("making lists of signal, geocoords", flush=True)
signal = []
latlng = []
shops_list = []
for geo_node in geo_nodes:
    lat = geo_node[2]
    lng = geo_node[3]
    update_ratio = make_update_ratio(geo_node[1])
    latlng.append([lat, lng])
    signal.append(update_ratio)
    shops_list.append(geo_node[1])
print("geocoods num =", len(latlng), flush=True)

# 計算時間計測
end = dt.datetime.today()
print("making lists =", end - start, flush=True)
start = dt.datetime.today()

# signalの標準偏差計算
f = []
average = sum(signal) / len(signal)
value = 0
for sig in signal:
    value += (sig - average)**2
SD = math.sqrt(value / len(signal))
for sig in signal:
    hoge = (sig - average) / (3*SD)
    if hoge > 1.0:
        hoge = 1.0
    if hoge < -1.0:
        hoge = -1.0
    f.append(hoge)

# signal直接
#f = []
#for sig in signal:
#    f.append(sig)

print(average)
print(SD)

# latlng:緯度経度 → coords:直交座標
print("geocoods → xy coords", flush=True)
coords = []
EPSG4612 = pyproj.Proj("+init=EPSG:4612")
EPSG2449 = pyproj.Proj("+init=EPSG:2449")
for ltln in latlng:
    y,x = pyproj.transform(EPSG4612, EPSG2449, ltln[1],ltln[0])
    coords.append([x, y])

# 計算時間計測
end = dt.datetime.today()
print("coordinate transformation =", end - start, flush=True)
start = dt.datetime.today()

# Wの作成
print("making weight matrix", flush=True)
W = np.zeros((len(coords), len(coords)), dtype='float16')

# 全結合
for i in range(len(coords)):
    for j in range(len(coords)):
        W[i, j] = comupute_inverse_distance(latlng[i], latlng[j])
np.fill_diagonal(W, 0.0)

# Wの正規化
L = normalization_matrix(W)

# 計算時間計測
end = dt.datetime.today()
print("made weight matrix =", end - start, flush=True)
start = dt.datetime.today()

# save csv files
print("saving files", flush=True)
np.savetxt("./csv_files/W.csv", W, delimiter=",")
np.savetxt("./csv_files/L.csv", L, delimiter=",")
np.savetxt("./csv_files/coords.csv", coords, delimiter=",")
np.savetxt("./csv_files/latlng.csv", latlng, delimiter=",")
np.savetxt("./csv_files/f.csv", f, delimiter=",")
np.savetxt("./csv_files/serials.csv", shops_list, delimiter=",")

# 計算時間計測
end = dt.datetime.today()
print("saving files =", end - start, flush=True)

print("end", flush=True)
# 切断
cur.close()
conn.close()