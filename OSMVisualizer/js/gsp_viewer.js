// htmlファイルがあるディレクトリでサーバー起動
// python3 -m http.server 8000
// 起動時urlにパラメータ付与
// http://localhost:8000/gsp.html?json=data/gsp.json

// parse url params
var arg = new Object;
var pair=location.search.substring(1).split('&');
for(var i=0;pair[i];i++) {
    var kv = pair[i].split('=');
    arg[kv[0]]=kv[1];
}
console.log(arg)

// Constants
var MAP_ID = 'map'
var JSON_FILE = arg.json

//　描画したレイヤーの保存
var myLayers =[];
var pointLayers=[];
var pointFeatures =[];
var lineLayers = [];

// signalとfilteredの閾値定義用
var f = [];
var f2 = [];
var f_thresh = 0;
var f2_thresh = 0;
var thresh_rate = 5;

// mapをグローバルで仮定義
var map;

// Main function for map
createMap = function(){
	fetchDataPointLayer().then(function(dataPointLayers){
		// base map setup
		var baseMap = new ol.layer.Tile({
			source: new ol.source.OSM(),
			name: "base"
		});
		var raster = new ol.layer.Tile({
			source: new ol.source.Stamen({
				layer: 'toner'
			}),
			name:"raster"
		});
		// 透過率の設定
		baseMap.setOpacity(0.5)
		raster.setOpacity(0.5)

		// set initial view point
		var view = new ol.View({
			center: ol.proj.transform([136.91025850, 35.14870960], "EPSG:4326", "EPSG:3857"),
			zoom: 12
		});

		// 作図用　栄らへん
		// var view = new ol.View({
		// 	center: ol.proj.transform([136.918532, 35.174548], "EPSG:4326", "EPSG:3857"),
		// 	zoom: 15
		// });

		// 作図用　名古屋市全体
		// var view = new ol.View({
		// 	// center: ol.proj.transform([137.120532, 35.174548], "EPSG:4326", "EPSG:3857"),
		// 	center: ol.proj.transform([137.120532, 35.176448], "EPSG:4326", "EPSG:3857"),
		// 	zoom: 12
		// });

		// create map object
		map = new ol.Map({
			target: MAP_ID,
			layers: [baseMap],
			view: view,
			renderer: ['canvas', 'dom'],
			controls: ol.control.defaults().extend([new ol.control.ScaleLine()]),
			interactions: ol.interaction.defaults()
		});
		// add zoom slider
		map.addControl(new ol.control.ZoomSlider());

		// add dataPointLayer on map
		// 反転させないと描画順の問題でポイントが見えなくなる
		dataPointLayers.reverse()
		for(var i = 0; i < dataPointLayers.length; i++){
			map.addLayer(dataPointLayers[i]);
		}

		// set click event on map // pointermove
		map.on('click', function(evt) {
			detailPoint(evt.pixel, evt.coordinate);
		});

		// click function
		detailPoint = function(pixel, coordinate) {
			var plotCount = 0;
			map.forEachFeatureAtPixel(pixel, function(feature, wp_layer) {
				console.log('siganl', feature.get('signal'));
				console.log('filterd', feature.get('filtered'));
				console.log('meta', feature.get('meta'));
			});
		};
	})
};

fetchDataPointLayer = function(){
	return new Promise(function(resolve, reject){
		loadJson(JSON_FILE).then(function(data){
			// layerは点をプロットした1層のみに対応
			var vectorSource = new ol.source.Vector('dataLayer');
			for(var i = 0; i < data.length; i++){
				var pos = data[i]['geometry']
				// 閾値判別
				// if(data[i]['filtered'] <= f2_thresh){
				// 	continue;
				// }
				// create feature
				var feature = new ol.Feature({
					geometry: new ol.geom.Point(pos, 'XY'),
					signal: Math.round(data[i]['signal']*100+100),
					filtered: Math.round(data[i]['filtered']*100+100),
					meta: data[i]['meta'],
					line: data[i]['line']
				});
				pointFeatures.push(feature);
				// set style
				feature.setStyle(styles[feature.get('signal')]);
				// feature.setStyle(lightStyle);
				// add feature
				vectorSource.addFeature(feature)
				// create line layer
				fetchLineLayer(feature.get('line')).then(function(lineLayer){
					// add lineLayer on map
					for(var j = 0; j < lineLayer.length; j++){
						pointLayers.push(lineLayer[j])
					}
				})
			}
			console.log(pointFeatures.length)
			// create layer
			var vectorLayer = new ol.layer.Vector();
			vectorLayer.setSource(vectorSource);
			pointLayers.push(vectorLayer)
			myLayers.push(vectorLayer)
			resolve(pointLayers)
		});
	});
};

fetchLineLayer = function(lineStrArray){
	return new Promise(function(resolve, reject){
		var vectorLayers = []
		for (var i = 0; i < lineStrArray.length; i++){
			var lineString = new ol.geom.LineString(lineStrArray[i]);
			// create feature
			var vectorSource  = new ol.source.Vector({
				features: [new ol.Feature({
					geometry: lineString,
					name: 'Line',
				})]
			});
			// set style
			lineStyle = lineStyleGray
			// create layer
			var vectorLayer = new ol.layer.Vector({
				source: vectorSource,
				style: lineStyle,
			});
			vectorLayers.push(vectorLayer)
			lineLayers.push(vectorLayer)
		}
		resolve(vectorLayers)
	});
};

clearlineLayers = function(){
	console.log("clear lineLayers")
	for(var j = 0; j < lineLayers.length; j++){
		map.removeLayer(lineLayers[j]);
	}
}

addlineLayers = function(){
	console.log("add lineLayers")
	map.removeLayer(myLayers[0]);
	for(var j = 0; j < lineLayers.length; j++){
		map.addLayer(lineLayers[j]);
	}
	map.addLayer(myLayers[0]);
}

//////////////////////////////
// Style
//////////////////////////////
// noneStyle
var noneStyle = new ol.style.Style({
	image: new ol.style.Circle({
		radius: 0.0,
		fill: new ol.style.Fill({color: 'black'})
	})
});

// defaultStyle
var defaultStyle = new ol.style.Style({
	image: new ol.style.Circle({
		radius: 3.0,
		fill: new ol.style.Fill({color: 'blue'})
	})
});

// lightStyle
var lightStyle = new ol.style.Style({
	image: new ol.style.Circle({
		radius: 20.0,
		fill: new ol.style.Fill({color: 'blue'})
	})
});

// line color styles
var lineStyleGray = new ol.style.Style({
	stroke: new ol.style.Stroke({ color: 'gray', width: 2 })
});
var lineStyleRed = new ol.style.Style({
	stroke: new ol.style.Stroke({ color: 'red', width: 2 })
});

// make heatmap color styles
// https://www.peko-step.com/html/hsl.html
// h= 0° : red ~ 240° : blue
// ratioが1に近いほど赤, 0は青
var styles=[];
function heatMapColorforValue(value){
	var h = (200 - value)*1.2;
	return "hsl(" + h + ", 100%, 50%)";
}

for(var j = 0; j < 201; j++){
	styles.push(
		new ol.style.Style({
			image: new ol.style.Circle({
				radius: 3.0,
				fill: new ol.style.Fill({color: heatMapColorforValue(j)})
			})
		})
	);
}

// 数値用の比較関数を定義
function compareNumbers(a, b) {
  return a - b;
}

//////////////////////////////
// load File
//////////////////////////////
loadJson = function(file){
	return new Promise(function(resolve, reject){

		var data;

		console.log("load: " + file);
		$.getJSON(file , function(d) {
			data = [];
			for(var i = 0; i < d.length; i++){
				var geometry = ol.proj.transform(d[i]['coordinate'], "EPSG:4326", "EPSG:3857");
				var filtered = d[i]['filtered'];
				var signal = d[i]['signal'];
				var meta = d[i]['meta'];
				var lineArray = d[i]['line']
				var line = [];
				f.push(signal)
				f2.push(filtered)
				if(filtered > 1.0){
					filtered = 1.0
				}
				for(var j = 0; j < lineArray.length; j++){
					var terminal = ol.proj.transform(lineArray[j], "EPSG:4326", "EPSG:3857");
					line.push([geometry, terminal])
				}
				data.push({geometry: geometry, filtered: filtered, signal: signal, meta: meta, line: line})
			}
			f = f.sort(compareNumbers);
			f2 = f2.sort(compareNumbers);
			f_thresh = f[f.length-Math.round(f.length*thresh_rate/100)]
			f2_thresh = f2[f2.length-Math.round(f2.length*thresh_rate/100)]
			console.log('f_thresh =',f_thresh)
			console.log('f2_thresh =',f2_thresh)
			resolve(data)
		});
	});
};