# -*- coding: utf-8 -*-
"""
csv_fileを読み込んでnavimapでプロットできるjsonファイルの作成
scp yossi@133.6.254.14:./gsp/gsp.json ./
"""

import psycopg2
import json
import datetime as dt
import csv
import numpy as np

# 境目の日付を定義
update_keys = []
for i in range(1,10):
    update_keys.append('2017-12-0' + str(i))
for i in range(10,32):
    update_keys.append('2017-12-' + str(i))


def open_with_numpy_loadtxt(filename):
    """ csvファイルの読み込み In:ファイルパス, Out:np.ndarray """
    return np.loadtxt(filename, delimiter=',')


def make_update_ratio(shops_serial):
    cnt = 0
    for day in update_keys:
        begin = dt.datetime.strptime(day, '%Y-%m-%d')
        end = begin + dt.timedelta(days=1)
        cur.execute("SELECT distinct_alphanumeric FROM html_log WHERE update_time >= %s AND update_time <= %s AND shops_serial = %s;",[begin, end, shops_serial])
        ratio_tupple = cur.fetchone()
        if ratio_tupple:
            if ratio_tupple[0] != 1.0:
                cnt += 1
    return cnt/len(update_keys)


def make_meta_dict(shops_serial):
    """ 引数から辞書オブジェクトを作成しreturn """
    cur.execute("SELECT serial, url, title, category FROM shops_hp WHERE serial = %s;",[shops_serial])
    shop_meta = cur.fetchone()
    ratio = make_update_ratio(shops_serial)
    return {
            "shops_serial": shop_meta[0],
            "url": shop_meta[1],
            "title": shop_meta[2],
            "ratio": ratio,
            "category": shop_meta[3]
            }

def make_geo_dict(index, latlng, meta, line):
    """ 引数から辞書オブジェクトを作成しreturn """
    return {
            "coordinate": [latlng[1], latlng[0]],
            "signal": f[index],
            "filtered": f2[index],
            "line": line,
            "meta": meta
            }

# PostgreSQLに接続
try:
    conn = psycopg2.connect("dbname='placesdb' user='yossi' host='localhost' password='hoge'")
    cur = conn.cursor()
except:
    print("接続失敗")

start = dt.datetime.today()
# 重み行列W, 地理座標latlng, 信号値f, の読み込み 
print("reading files", flush=True)
W = open_with_numpy_loadtxt('./csv_files/W.csv')
latlng_list = open_with_numpy_loadtxt('./csv_files/latlng.csv')
f = open_with_numpy_loadtxt('./csv_files/f.csv')
f2 = open_with_numpy_loadtxt('./csv_files/f9.csv')
serials = open_with_numpy_loadtxt('./csv_files/serials.csv')

# 計算時間計測
end = dt.datetime.today()
print("read files =", end - start, flush=True)
start = dt.datetime.today()

print("making dicts", flush=True)
geo_list = []
for i, latlng in enumerate(latlng_list):
    # metaの作成
    # tmp = make_update_ratio(serials[i])
    meta = []
    # meta_dict = make_meta_dict(serials[i])
    # meta.append(meta_dict)
    # lineの作成
    line = []
#    for j, weight in enumerate(W[i]):
#        lat = latlng_list[j][0]
#        lng = latlng_list[j][1]
#        line.append([lng, lat])
    # line = list(set(line))

    # geo_dictの作成
    geo_dict = make_geo_dict(i, latlng, meta, line)
    geo_list.append(geo_dict)

# 計算時間計測
end = dt.datetime.today()
print("made dicts =", end - start, flush=True)
start = dt.datetime.today()

print("making json file", flush=True)
with open("gsp.json", "w") as f:
    json.dump(geo_list, f, ensure_ascii=False, indent=4, sort_keys=True, separators=(',', ': '))

# 計算時間計測
end = dt.datetime.today()
print("made json file =", end - start, flush=True)
start = dt.datetime.today()

print("end", flush=True)
# 切断
cur.close()
conn.close()