# -*- coding: utf-8 -*-
"""
ratio計算　更新差分テキストdiff取得
まとめてhtml取得するとメモリ圧迫するから小分けにselect
"""

import urllib.request as ur
import urllib.parse as up
from bs4 import BeautifulSoup
import psycopg2
import datetime
from difflib import SequenceMatcher
import re
import json

# 英数字の削除パターン, コンパイル
p_alnum = re.compile(r"\w")
p_space = re.compile(r"\s")
p_symbol = re.compile(r"[!-~]")
p_else = re.compile(r"(\–|\…|\’|\¸)")


def shaping_html(text):
    p_tag = re.compile(r"<[^>]*?>", flags=(re.MULTILINE | re.DOTALL))
    p_space = re.compile(r"\s", flags=(re.MULTILINE | re.DOTALL))
    text = p_tag.sub('',  text)
    text = p_space.sub('', text)
    return text


def make_diff_dict(diff):
    delete_list = []
    insert_list = []
    replace_list = []
    for d in diff:
        if d[0] == 'equal':
            continue
        if d[0] == 'delete':
            delete_list.append(seq1[d[1]:d[2]:])
        if d[0] == 'insert':
            insert_list.append(seq2[d[3]:d[4]:])
        if d[0] == 'replace':
            replace_list.append([seq1[d[1]:d[2]:], seq2[d[3]:d[4]:]])
    return {"delete": delete_list,
            "insert": insert_list,
            "replace": replace_list}


def check_alphanumeric(diff, ratio):
    # diffのチェック
    delete = ''.join(diff["delete"])
    insert = ''.join(diff["insert"])
    replace_list = diff["replace"]
    replace = ''
    if len(replace_list) != 0:
        for tmp in replace_list:
            hoge = ''.join(tmp)
            replace = replace + hoge
    text = delete + insert + replace
    text = p_alnum.sub('', text)
    text = p_space.sub('', text)
    text = p_symbol.sub('', text)
    text = p_else.sub('', text)
    if text == '':
        print('detect inappropriate update', flush=True)
        ratio = 1.0
    return ratio


#PostgreSQLに接続
try:
    conn = psycopg2.connect("dbname='placesdb' user='yossi' host='localhost' password='hoge'")
    cur = conn.cursor()
except:
    print("接続失敗")

#住所未探索アドレス取得
cur.execute("SELECT serial, previous_id FROM html_log WHERE ratio = -1.0 ORDER BY update_time ASC;")
nodes = cur.fetchall()
print("start", flush=True)
print("探索アドレス数:", len(nodes), flush=True)
for node in nodes:
    print(node[0], flush=True)
    # beforeの取得
    cur.execute("SELECT html FROM html_log WHERE serial = %s;",[node[1]])
    before = cur.fetchone()
    # afterの取得
    cur.execute("SELECT html FROM html_log WHERE serial = %s;",[node[0]])
    after = cur.fetchone()

    seq1 = shaping_html(before[0])
    seq2 = shaping_html(after[0])
    # difflibによるratioとdiffの算出
    matcher = SequenceMatcher()
    matcher.set_seq1(seq1)
    matcher.set_seq2(seq2)
    ratio = matcher.ratio()
    diff = list(matcher.get_opcodes())
    diff_dict = make_diff_dict(diff)
    da_ratio = check_alphanumeric(diff_dict, ratio)
    diff_json = json.dumps(diff_dict)

    # html_logのアップデート
    sql = """UPDATE html_log SET ratio = %s, diff = %s, distinct_alphanumeric = %s WHERE serial = %s;"""
    cur.execute(sql,[ratio, diff_json, da_ratio, node[0]])
    conn.commit()

print("end", flush=True)
#切断
cur.close()
conn.close()