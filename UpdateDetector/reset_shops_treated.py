# -*- coding: utf-8 -*-
"""
shops_hpのtreatedを全部Falseに
要実行待ち
"""

import psycopg2

#PostgreSQLに接続
try:
    conn = psycopg2.connect("dbname='placesdb' user='yossi' host='localhost' password='hoge'")
    cur = conn.cursor()
except:
    print("接続失敗")

sql = """UPDATE shops_hp SET treated = false;"""
cur.execute(sql)
conn.commit()

#切断
cur.close()
conn.close()
