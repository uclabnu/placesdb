# -*- coding: utf-8 -*-
"""
各Webページのアップデート検出
treatedを全部Falseにしてから起動すること
update shops_hp set treated = false;
もしくはreset_shops_treated.pyを起動
"""

import urllib.request as ur
import urllib.error as ue
import urllib.parse as up
from bs4 import BeautifulSoup
import psycopg2
import datetime

# PostgreSQLに接続
try:
    conn = psycopg2.connect("dbname='placesdb' user='yossi' host='localhost' password='hoge'")
    cur = conn.cursor()
except:
    print("接続失敗")

cnt = 0
cnt_max = 1000
ratio = -1.0

print('start', flush=True)
while cnt < cnt_max:
    # 未チェックノード
    cur.execute("SELECT serial, url, html, last_update FROM shops_hp WHERE treated = false AND error != 'Unknown' ORDER BY random();")
    node = cur.fetchone()
    cnt += 1

    if node is None:
        break

    sql = """UPDATE shops_hp SET treated = true WHERE serial = %s;"""
    cur.execute(sql,[node[0]])
    conn.commit()
    print(node[0], flush=True)

    # アクセス日時の取得
    today = datetime.datetime.now() + datetime.timedelta(hours=9)    #計算開始時刻+9時間して日本標準時に
    try:    # urlが開けない場合のエラー回避
        html = ur.urlopen(node[1])
    except ue.HTTPError as e:
        print("Encountered an error: URLError at " + node[1], flush=True)
        print("{0} {1}".format(e.code, e.reason), flush=True)
        sql = """UPDATE shops_hp SET error = 'URLError' WHERE serial = %s;"""
        cur.execute(sql,[node[0]])
        conn.commit()
        continue
    except UnicodeEncodeError as e:
        print("Encountered an error: UnicodeEncodeError at " + node[1], flush=True)
        sql = """UPDATE shops_hp SET error = 'UnicodeEncodeError' WHERE serial = %s;"""
        cur.execute(sql,[node[0]])
        conn.commit()
        continue
    except:
        import traceback
        print("")
        print("Encountered an error:at " + node[1], flush=True)
        print(traceback.format_exc(), flush=True)
        print("")
        continue

    soup = BeautifulSoup(html,"lxml")
    after = soup.prettify()
    before = node[2]

    if before == after:
        continue

    # previous_idの取得
    cur.execute("SELECT serial FROM html_log WHERE shops_serial = %s ORDER BY update_time DESC;",[node[0]])
    previous = cur.fetchone()

    # インサート
    sql = """INSERT INTO html_log(shops_serial, url, html, update_time, ratio, previous_id) VALUES (%s, %s, %s, %s, %s, %s);"""
    cur.execute(sql,[node[0], node[1], after, today, ratio, previous[0]])
    sql = """UPDATE shops_hp SET html = %s, last_update = %s WHERE serial = %s;"""
    cur.execute(sql,[after, today, node[0]])
    conn.commit()

print('end', flush=True)
# 切断
cur.close()
conn.close()