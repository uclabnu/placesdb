# -*- coding: utf-8 -*-
"""
幅優先探索
アウトバウンドリンク検出の際はトップページを取得
トップページから深さ2までの探索を終えたら次のトップページへ
"""

import urllib.request as ur
import urllib.parse as up
import urllib.error as ue
from bs4 import BeautifulSoup, SoupStrainer
import psycopg2
import datetime
import time
import re
import socket

def crawl(node):
    """ 引数はタプル node[0]=id, node[1]=url, node[2]=layer, node[3]=root """

    # アクセス日時の取得
    today = datetime.datetime.now() + datetime.timedelta(hours=9)    # 計算開始時刻+9時間して日本標準時に
    
    # treating更新
    sql = """UPDATE main SET version = %s, treating = true WHERE id = %s;"""
    cur.execute(sql,[version, node[0]])
    conn.commit()

    # domainテーブル更新
    o = up.urlparse(node[1])
    check_domain(o.netloc, today)

    try:    # urlが開けない場合のエラー回避
        html = ur.urlopen(node[1])
    except ue.HTTPError as e:
        print("Encountered an error: URLError at " + node[1], flush=True)
        print("{0} {1}".format(e.code, e.reason), flush=True)
        return
    except UnicodeEncodeError as e:
        print("Encountered an error: UnicodeEncodeError at " + node[1], flush=True)
        return
    except:
        import traceback
        print("", flush=True)
        print("Encountered an error:at " + node[1], flush=True)
        print(traceback.format_exc(), flush=True)
        print("", flush=True)
        return

    # SoupStrainerでaタグのみ抽出
    links=[]
    for link in BeautifulSoup(html, "lxml", parse_only=SoupStrainer("a", href=True)).find_all("a"):
        tmp = up.urljoin(node[1], link["href"])
        links.append(tmp)
    links = list(set(links))    # 重複削除

    # mainテーブルにリンク先urlを追加
    for link in links:
        insert_links(link, node[2], node[3], o.netloc)
    conn.commit()

    # linksテーブルにリンク情報を記入
    for link in links:
        update_links_table(link, node[0])
    conn.commit()
    
    # mainのアップデート
    sql = """UPDATE main SET treated = true, access_dt = %s WHERE id = %s;"""
    cur.execute(sql,[today, node[0]])
    conn.commit()

    if node[2] == 0:
        # 下層nodeのselect
        cur.execute("SELECT id, url, layer, root FROM main WHERE root = %s AND layer = 1;",[node[3]])
        underlayers = cur.fetchall()
        for underlayer in underlayers:
            crawl(underlayer)
    return


def insert_links(url, layer, root, domain):
    """ 引数のurlが外部ドメインかどうか判断してmainへ重複を避けてinsert """

    o = up.urlparse(url)
    if o.netloc == domain:
        sql = """INSERT INTO main(url, root, layer) SELECT %s, %s, %s WHERE NOT EXISTS (SELECT id FROM main WHERE url = %s);"""
        cur.execute(sql,[url, root, layer+1, url])
    else:
        # toppageの仮想定義　アクセスできない場合はリターン
        toppage = 'http://' + o.netloc
        error = ''
        try:    # urlが開けない場合のエラー回避
            html = ur.urlopen(toppage)
        except ue.HTTPError as e:
            print("Encountered an error: URLError at " + toppage, flush=True)
            print("{0} {1}".format(e.code, e.reason), flush=True)
            error = 'URL Error'
        except UnicodeEncodeError as e:
            print("Encountered an error: UnicodeEncodeError at " + toppage, flush=True)
            error = 'UnicodeEncode Error'
        except:
            import traceback
            print("", flush=True)
            print("Encountered an error:at " + toppage, flush=True)
            print(traceback.format_exc(), flush=True)
            print("", flush=True)
            error = 'Unknown Error'

        # toppageを入れた後通常のインサートをNOT EXISTで行う
        if error == '':
            sql = """INSERT INTO main(url) SELECT %s WHERE NOT EXISTS (SELECT id FROM main WHERE url = %s);"""
            cur.execute(sql,[toppage, toppage])
            conn.commit()
            cur.execute("SELECT id FROM main WHERE url = %s;",[toppage])
            root_id = cur.fetchone()
            sql = """UPDATE main SET layer = 0, root = %s, toppage = true WHERE url = %s;"""
            cur.execute(sql,[root_id[0], toppage])
        sql = """INSERT INTO main(url, root, layer) SELECT %s, %s, %s WHERE NOT EXISTS (SELECT id FROM main WHERE url = %s);"""
        cur.execute(sql,[url, root, layer+1, url])


def update_links_table(url, parent):
    """ リンク情報をlinksテーブルに記入 """

    cur.execute("SELECT id FROM main WHERE url = %s;",[url])
    child = cur.fetchone()    # childはタプル 要素は1つ
    # linksに入力
    sql = """INSERT INTO links(parent, child) VALUES (%s, %s);"""
    cur.execute(sql,[parent, child[0]])


def check_domain(domain, today):
    """ ドメインテーブルの更新とタイムスリープ """

    # 新規ドメインの入力
    sql = """INSERT INTO domain(domain, access_dt) SELECT %s, %s WHERE NOT EXISTS (SELECT id FROM domain WHERE domain = %s);"""
    cur.execute(sql,[domain, today, domain])
    conn.commit()
    # 集中アクセス回避
    cur.execute("SELECT access_dt FROM domain WHERE domain = %s;",[domain])
    dt = cur.fetchone()    #dtはタプル 要素は1つ
    timedelta = today - dt[0]
    if timedelta < datetime.timedelta(seconds=2):    # 2秒以内にアクセスしていた場合5秒スリープ（要調整）
        time.sleep(5)
    # 既存ドメインのアップデート
    sql = """UPDATE domain SET access_dt = %s WHERE domain = %s;"""
    cur.execute(sql,[today, domain])
    conn.commit()


# PostgreSQLに接続
try:
    conn = psycopg2.connect("dbname='crawlerdb' user='yossi' host='localhost' password='hoge'")
    cur = conn.cursor()
except:
    print("接続失敗")

# パラメータ定義
version = 'ver 2.1.1'
cnt = 0
cnt_max = 100

# IPv4優先
origGetAddrInfo = socket.getaddrinfo
def getAddrInfoWrapper(host, port, family=0, socktype=0, proto=0, flags=0):
    return origGetAddrInfo(host, port, socket.AF_INET, socktype, proto, flags)
# replace the original socket.getaddrinfo by our version
socket.getaddrinfo = getAddrInfoWrapper

# クロール開始
print('crawl start', flush=True)
while cnt < cnt_max:
    # 未チェックノード検索
    cur.execute("SELECT id, url, layer, root FROM main WHERE treating = false AND toppage = true;")
    next_node = cur.fetchone()
    print(next_node[0], next_node[1], flush=True)
    crawl(next_node)
    cnt += 1
print('end', flush=True)

# 切断
cur.close()
conn.close()