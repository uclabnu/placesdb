# -*- coding: utf-8 -*-
"""
正規表現のパターンマッチングにより住所情報を取得
緯度経度に変換した後geoにインサート
"""

import urllib.request as ur
import urllib.parse as up
import urllib.error as ue
from bs4 import BeautifulSoup
import re
import psycopg2
import json

def serch_address(text, node):
    """ 全文から住所探索し住所情報を抽出 """
    match = p0.findall(text)    # 住所のおおまかな探索
    if match:
        for before in match:
            after = ""
            text = re.split(split1, before)[0]    # 空白文字と句読点でスプリット
            match = p1.findall(text)
            if match:
                after = match[0]
            else:
                text = re.split(split2, before)    # 数字とそれ以外の境界でスプリット
                text = "".join(text[1:3:])
                match = p1.findall(text)
                if match:
                    after = match[0]
            # 住所があった場合geocoding
            if after:
                geocode(after, node)


def geocode(address, node):
    """ 住所から緯度経度に変換しgeoにインサート """
    response = yapi_geo(address)
    content = json.loads(response.read().decode('utf8'))
    if content.get('Feature'):    #ジオコーディングの可否による分岐
        lnglat = content["Feature"][0]["Geometry"]["Coordinates"].split(",")
    else:
        lnglat = [0.0, 0.0]
    sql = """INSERT INTO geo(id, url, address, lat, lng) SELECT %s, %s, %s, %s, %s WHERE NOT EXISTS (SELECT serial FROM geo WHERE id = %s AND lat = %s AND lng = %s);"""
    cur.execute(sql,[node[0], node[1], address, lnglat[1], lnglat[0], node[0], lnglat[1], lnglat[0]])
    conn.commit()


def yapi_geo(address):
    url = 'http://geo.search.olp.yahooapis.jp/OpenLocalPlatform/V1/geoCoder?'
    appid = 'dj0zaiZpPVVNNnV6NlFKRTdTTSZzPWNvbnN1bWVyc2VjcmV0Jng9ZjU-'
    address = up.quote(address.encode('utf-8'))
    geo_url = url + 'appid=' + appid + '&query=' + address + '&output=json'
    response = ur.urlopen(geo_url)
    return response


# 住所探索パターン一覧
pattern = r"名古屋市(?:東|西|南|北|中|港|緑|守山|千種|昭和|中村|瑞穂|熱田|中川|名東|天白)区"
pattern0 = pattern + ".{0,40}"
pattern1 = pattern + ".*[0-9０１２３４５６７８９号地先]$"
p0 = re.compile(pattern0)
p1 = re.compile(pattern1)
split1 = r'[\s　、,，]'
split2 = r'([^0-9-０１２３４５６７８９ー(丁目)(番地)番号のがは]+)([0-9-０１２３４５６７８９ー(丁目)(番地)番号]+)'

# PostgreSQLに接続
try:
    conn = psycopg2.connect("dbname='crawlerdb' user='yossi' host='localhost' password='hoge'")
    cur = conn.cursor()
except:
    print("接続失敗")

# 住所未探索アドレス取得
cur.execute("SELECT id, url FROM main WHERE search_address = false ORDER BY random() LIMIT 1000;")
nodes = cur.fetchall()
print("start", flush=True)
print("探索アドレス数:", len(nodes), flush=True)

for node in nodes:
    print(node[1], flush=True)
    try:    # urlが開けない場合のエラー回避
        html = ur.urlopen(node[1])
    except ue.HTTPError as e:
        print("Encountered an error: URLError at " + node[1], flush=True)
        print("{0} {1}".format(e.code, e.reason), flush=True)
        continue
    except UnicodeEncodeError as e:
        print("Encountered an error: UnicodeEncodeError at " + node[1], flush=True)
        continue
    except:
        import traceback
        print("", flush=True)
        print("Encountered an error:at " + node[1], flush=True)
        print(traceback.format_exc(), flush=True)
        print("", flush=True)
        continue

    soup = BeautifulSoup(html,"lxml")
    title = soup.find("title")    # titleの取得
    if title is None:    # titleが取得できない場合のエラー回避
        title = 'Title Error'
    else:
        title = title.string

    text = soup.prettify()    # 全文取得
    serch_address(text, node)

    # mainのアップデート
    sql = """UPDATE main SET title = %s, search_address = true WHERE id = %s;"""
    cur.execute(sql,[title, node[0]])
    conn.commit()

print("end", flush=True)
# 切断
cur.close()
conn.close()